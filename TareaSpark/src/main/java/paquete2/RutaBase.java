
package paquete2;


import spark.Route;

public abstract class RutaBase implements Route {

    public String path;

    protected RutaBase(String path) {
        this.path = path;
    }
}



package paquete3;

import paquete2.RutaBase;
import spark.Request;
import spark.Response;
import java.io.File;
import java.nio.file.Files;


public class Principal extends RutaBase {
    
    public Principal() {
        super("/");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        return new String(Files.readAllBytes(new File("html/Principal.html").toPath())); }
    
    
    
}

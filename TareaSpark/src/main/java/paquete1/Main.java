package paquete1;

import paquete2.RutaBase;
import paquete3.Datos;
import paquete3.GetDatos;
import paquete3.Principal;
import spark.Spark;



public class Main {
  
    public static void main(String[] args) {
      Spark.port(8090);
      addRoute(new Principal());
      addRoute(new Datos());
      addRoute(new GetDatos());
      Spark.init();  
        
    }   
   private static void addRoute(RutaBase rutaBase) {
    Spark.get(rutaBase.path, rutaBase);
}
}
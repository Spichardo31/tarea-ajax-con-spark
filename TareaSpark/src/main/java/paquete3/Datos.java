
package paquete3;

import paquete2.RutaBase;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.List;

public class Datos extends RutaBase {
    
    
    public Datos() {
        super("/prueba");
    }

    @Override
    public Object handle(Request rqst, Response rspns) throws Exception {
       String result = "";
        List<String> productos = produc();
        for (int i =0; i < productos.size(); i++) {
        result += "<br>" + "Id de producto: " + i + " Nombre de Producto: " + productos.get(i) + "<br>";
        }
        return result;
    
    }
public static List<String> produc() {
    
        List<String> productos = new ArrayList<>();
        
        productos.add("Poloche");
        productos.add("Camisa");
        productos.add("Jeans");
        productos.add("Medias");
        productos.add("Tenis");
        
        return productos;
    }
   
    
}

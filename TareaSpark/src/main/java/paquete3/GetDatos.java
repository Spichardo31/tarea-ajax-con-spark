
package paquete3;

import spark.Request;
import spark.Response;
import paquete2.RutaBase;
import static paquete3.Datos.produc;



public class GetDatos extends RutaBase {
    
     public GetDatos() {
        super("/prueba/:id");
    }

    @Override
    public Object handle(Request request, Response rspns) throws Exception {
        
    try {
            String ID = request.params(":id");
            if (ID != null && !ID.isEmpty()) {
                return Datos.produc().get(Integer.parseInt(ID));
            }
            return "No Param";
        } catch (Exception ignored) {
            return "ID no valido";
        }
    }
}